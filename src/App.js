import React, { Component } from 'react';
import {connect} from "react-redux";

import './App.css';
import FA from 'react-fontawesome';

import {changeFormField, decodeText, encodeText} from "./store/actions";

class App extends Component {
  render() {
    return (
      <div className="Main">
        <div className="MainRow">
          <div className="LeftCol">Decoded message</div>
          <div className="RightCol">
            <textarea name="decoded"
                      value={this.props.decoded}
                      onChange={(e) => this.props.onChangeFormField(e)}/>
          </div>
        </div>
        <div className="MainRow">
          <div className="LeftCol">Password</div>
          <div className="RightCol">
            <input type="password" name="password"
                   value={this.props.password}
                   onChange={(e) => this.props.onChangeFormField(e)}/>
            <span className="Arrow" title="Encode"
                  onClick={() => this.props.toEncodeText(this.props.decoded)}>
              <FA name="arrow-down"/>
            </span>
            <span className="Arrow" title="Decode"
                  onClick={() => this.props.toDecodeText(this.props.encoded)}>
              <FA name="arrow-up"/>
            </span>
          </div>
        </div>
        <div className="MainRow">
          <div className="LeftCol">Encoded message</div>
          <div className="RightCol">
            <textarea name="encoded"
                      value={this.props.encoded}
                      onChange={(e) => this.props.onChangeFormField(e)}/>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    decoded: state.decoded,
    encoded: state.encoded,
    password: state.password
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onChangeFormField: (e) => dispatch(changeFormField(e)),
    toDecodeText: (value) => dispatch(decodeText(value)),
    toEncodeText: (value) => dispatch(encodeText(value))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);