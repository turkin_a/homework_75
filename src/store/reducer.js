import * as actionTypes from './actionTypes';

const initialState = {
  decoded: '',
  encoded: '',
  password: ''
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_FORM_FIELD:
      let clearField = null;
      if (action.field.target.name === 'encoded') clearField = 'decoded';
      if (action.field.target.name === 'decoded') clearField = 'encoded';
      return {...state, [action.field.target.name]: action.field.target.value, [clearField]: ''};
    case actionTypes.SET_DECODED_TEXT:
      return {...state, decoded: action.result};
    case actionTypes.SET_ENCODED_TEXT:
      return {...state, encoded: action.result};
    default:
      return state;
  }
};

export default reducer;