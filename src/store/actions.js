import * as actionTypes from './actionTypes';
import axios from '../axios-api';

export const changeFormField = field => {
  return {type: actionTypes.CHANGE_FORM_FIELD, field}
};

export const setDecodedText = result => {
  return {type: actionTypes.SET_DECODED_TEXT, result};
};

export const setEncodedText = result => {
  return {type: actionTypes.SET_ENCODED_TEXT, result};
};

export const decodeText = value => {
  return (dispatch, getState) => {
    if (!getState().password || !value) return null;
    return axios.post('/decode', {password: getState().password, message: value})
      .then(response => dispatch(setDecodedText(response.data.decoded)))
      .catch(err => console.log(err));
  }
};

export const encodeText = value => {
  return (dispatch, getState) => {
    if (!getState().password || !value) return null;
    return axios.post('/encode', {password: getState().password, message: value})
      .then(response => dispatch(setEncodedText(response.data.encoded)))
      .catch(err => console.log(err));
  }
};